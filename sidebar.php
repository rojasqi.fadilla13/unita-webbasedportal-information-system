<?php
	if(!defined("INDEX")) die("---");
?>
<div class="box">

	<!-- 1 /-->
	<ul> &nbsp;&nbsp;
	<script type="text/javascript">
			//fungsi displayTime yang dipanggil di bodyOnLoad dieksekusi tiap 1000ms = 1detik
			function tampilkanwaktu(){
					//buat object date berdasarkan waktu saat ini
					var waktu = new Date();
					//ambil nilai jam,
					//tambahan script + "" supaya variable sh bertipe string sehingga bisa dihitung panjangnya : sh.length
					var sh = waktu.getHours() + "";
					//ambil nilai menit
					var sm = waktu.getMinutes() + "";
					//ambil nilai detik
					var ss = waktu.getSeconds() + "";
					//tampilkan jam:menit:detik dengan menambahkan angka 0 jika angkanya cuma satu digit (0-9)
					document.getElementById("clock").innerHTML = (sh.length==1?"0"+sh:sh) + ":" + (sm.length==1?"0"+sm:sm) + ":" + (ss.length==1?"0"+ss:ss);
			}
	</script>
	<body onload="tampilkanwaktu();setInterval('tampilkanwaktu()', 1000);">
	<span id="clock"></span>
	<?php
	$hari = date('l');
	/*$new = date('l, F d, Y', strtotime($Today));*/
	if ($hari=="Sunday") {
		echo "Minggu";
	}elseif ($hari=="Monday") {
		echo "Senin";
	}elseif ($hari=="Tuesday") {
		echo "Selasa";
	}elseif ($hari=="Wednesday") {
		echo "Rabu";
	}elseif ($hari=="Thursday") {
		echo("Kamis");
	}elseif ($hari=="Friday") {
		echo "Jum'at";
	}elseif ($hari=="Saturday") {
		echo "Sabtu";
	}
	?>,
	<?php
	$tgl =date('d');
	echo $tgl;
	$bulan =date('F');
	if ($bulan=="January") {
		echo " Januari ";
	}elseif ($bulan=="February") {
		echo " Februari ";
	}elseif ($bulan=="March") {
		echo " Maret ";
	}elseif ($bulan=="April") {
		echo " April ";
	}elseif ($bulan=="May") {
		echo " Mei ";
	}elseif ($bulan=="June") {
		echo " Juni ";
	}elseif ($bulan=="July") {
		echo " Juli ";
	}elseif ($bulan=="August") {
		echo " Agustus ";
	}elseif ($bulan=="September") {
		echo " September ";
	}elseif ($bulan=="October") {
		echo " Oktober ";
	}elseif ($bulan=="November") {
		echo " November ";
	}elseif ($bulan=="December") {
		echo " Desember ";
	}
	$tahun=date('Y');
	echo $tahun;
	?>
</ul>
<br>
	<!-- 2 /-->
	<h3 class="judul">PENCARIAN</h3>
	<form method="post" action="?tampil=pencarian">
	&nbsp;&nbsp;&nbsp;<input type="text" name="kata" placeholder="Cari di sini..."> <input type="submit" value="Cari">
	</form>

	<!-- 3 /-->
	<h3 class="judul">ARTIKEL TERBARU</h3>
	<ul>
		<?php
			$artikel = mysql_query("select * from artikel order by id_artikel desc limit 3");
			while($data=mysql_fetch_array($artikel)){
				echo "<li><a href='?tampil=artikel_detail&id=$data[id_artikel]'>$data[tanggal], $data[judul]</a></li>";
			}
		?>
	</ul>

	<!-- 4 /-->
	<h3 class="judul">INFO FAKULTAS</h3>
	<ul>
<li><a href="?tampil=pencarian_tekniksipil"> Teknik Sipil </a></li>
<li><a href="?tampil=pencarian_manajemen"> Manajemen </a></li>
<li><a href="?tampil=pencarian_pertanian"> Pertanian </a></li>
<li><a href="?tampil=pencarian_pendidikan"> Pendidikan </a></li>
<li><a href="?tampil=pencarian_hukum"> Ilmu Hukum </a></li></li>
	</ul>

	<!-- 5 /-->
	<h3 class="judul">AGENDA KEGIATAN</h3>
	<ul>
		<?php
			$artikel = mysql_query("select * from agenda order by id_agenda desc limit 5");
			while($data=mysql_fetch_array($artikel)){
				echo "<li><a href='?tampil=agenda_detail&id=$data[id_agenda]'>$data[tanggal], $data[judul]</a></li>";
			}
		?>
	</ul>

	<!-- 6 /-->
</div>
