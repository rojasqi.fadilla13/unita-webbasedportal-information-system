<?php
	session_start();

	include("../lib/koneksi.php");
	define("INDEX",true);
?>

<html>
	<head>
		<title>Halaman Administrator</title>
		<link rel="stylesheet" href="../css/admin.css">
	</head>
	<body>
	<div id="container">

	<div id="header">	</div>

	<div id="menu">
	<?php include("menu.php"); ?>
	</div>

	<div id="content">
	<?php include("konten.php"); ?>
	</div>

	<div id="footer">
	<p>Copyright 2017 &copy; Rojasqi Fadilla & Ojack Riyanto Butarbutar</p>

	<br>
<p>
<?php
	$tanggal= mktime(date("m"),date("d"),date("Y"));
	echo "".date("d-M-Y", $tanggal)."</b> ";
	date_default_timezone_set('Asia/Jakarta');
	$jam=date("H:i:s");
	echo "/ ". $jam." "."</b>";
	$a = date ("H");
	if (($a>=6) && ($a<=11)){
	echo "<b>, Selamat Pagi !!</b>";
	}
	else if(($a>11) && ($a<=15))
	{
	echo ", Selamat Pagi !!";}
	else if (($a>15) && ($a<=18)){
	echo ", Selamat Siang !!";}
	else { echo ", <b> Selamat Malam </b>";}
?>
</p>
  </div>
	</div>
	</body>
</html>
