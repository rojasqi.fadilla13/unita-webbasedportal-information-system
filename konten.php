<?php
	if(!defined("INDEX")) die("---");

	if( isset($_GET['tampil']) ) $tampil = $_GET['tampil'];
	else $tampil = "home";
?>

<div class='box'>

<?php
	if( $tampil == "home" )		include("konten/home.php");

elseif( $tampil == "halaman" )			include("konten/halaman.php");
	elseif( $tampil == "galeri" )			include("konten/galeri.php");
	elseif( $tampil == "alumni" )			include("konten/alumni.php");
	elseif( $tampil == "artikel_detail" )	include("konten/artikel_detail.php");
	elseif( $tampil == "agenda_detail" )	include("konten/agenda_detail.php");
	elseif( $tampil == "kontak" )			include("konten/kontak.php");
	elseif( $tampil == "kontak_proses" )	include("konten/kontak_proses.php");
	elseif( $tampil == "pencarian" )		include("konten/pencarian.php");
	elseif( $tampil == "pencarian_tekniksipil" )		include("konten/pencarian_tekniksipil.php");
	elseif( $tampil == "pencarian_manajemen" )		include("konten/pencarian_manajemen.php");
	elseif( $tampil == "pencarian_pertanian" )		include("konten/pencarian_pertanian.php");
	elseif( $tampil == "pencarian_pendidikan" )		include("konten/pencarian_pendidikan.php");
	elseif( $tampil == "pencarian_hukum" )		include("konten/pencarian_hukum.php");
	elseif( $tampil == "komentar_proses" )	include("konten/komentar_proses.php");

	else echo"Halaman tidak ditemukan";
?>

</div>
