-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 03, 2018 at 05:17 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unita`
--

-- --------------------------------------------------------

--
-- Table structure for table `agenda`
--

CREATE TABLE `agenda` (
  `id_agenda` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tempat` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `hits` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `agenda`
--

INSERT INTO `agenda` (`id_agenda`, `tanggal`, `judul`, `tempat`, `isi`, `gambar`, `hits`) VALUES
(47, '2017-12-08', 'ffdf', 'sssssssssssd', 'fffffffffffffffffffffffffd', 'login11.png', '14'),
(49, '2017-12-15', 'Seminar 212', 'Gedung L224', 'qwertyuiopp', 'buildMLirethil.PNG', '17');

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE `alumni` (
  `id_foto` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `deskripsi_foto` text NOT NULL,
  `tahun` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`id_foto`, `foto`, `nama`, `deskripsi_foto`, `tahun`) VALUES
(1, '1481353844557[1].jpg', 'Rojasqi Fadillah, S.Kom', 'Okelah', 2020),
(2, '1481353844557[1].png', 'Fadilla', 'dsafdsfdsfsdfsdfsdfsd', 2017),
(3, 'IMG_3519.jpg', 'Ridwan', 'Mantap', 2010),
(4, 'DSCF1526 merah-biru.jpg', 'Susi', 'edaninnn', 2005),
(5, 'IMG_0077.jpg', 'Zaenudini', 'thebesttlahh', 2007);

-- --------------------------------------------------------

--
-- Table structure for table `artikel`
--

CREATE TABLE `artikel` (
  `id_artikel` int(5) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `fakultas` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `hits` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `artikel`
--

INSERT INTO `artikel` (`id_artikel`, `judul`, `isi`, `gambar`, `fakultas`, `tanggal`, `hits`) VALUES
(2, 'Menciptakan Sarjana yang Percaya Diri Jadi Tantangan', 'Siborongborong - Universitas Sisingamangaraja XII Tapanuli Utara (Unita) menggelar wisuda 560 sarjana S1 di Kampus Bumi Pendidikan Silangit, Sabtu (9/8).\r\n\r\nTampak hadir, tokoh masyarakat Sumut Dr RE Nainggolan MM, Kepala Badan Pengembangan SDM Pendidikan dan Kebudayaan dan Penjaminan Mutu Pendidikan dari Kemendikbud, Prof Dr Syawal Gultom MPd yang menyampaikan Orasi Ilmiah, Bupati Tapanuli Utara dan yang mewakili Kopertis Wilayah I, Dra Elfriana MPd, dan utusan dari Pemkab se-Tapanuli.\r\n\r\nWisudawan/ti berasal dari Fakultas Teknik sebanyak 23 orang,  Fakultas Pertanian 17 orang, Fakultas Ekonomi 115 orang, Fakultas Hukum 42 orang, Fakultas Ilmu Pendidikan Prodi Matematika 80 orang, Prodi Bahasa Indonesia 124 orang, Prodi Bahasa Inggris 109 orang dan Prodi PPKN 50 orang.\r\n\r\nDiantara wisudawan/i, dua orang sarjana memperoleh penghargaan dari UNITA sebagai peraih IPK tertinggi dan lulusan termuda atas nama Herois Simaremare dari Fakultas FKIP Prodi Sastra Inggris, putri dari Manat Simaremare / Besti Br Rajagukguk, dengan IP 3,83. Sedangkan wisudawan termuda Dermin Simalango umur 22 Tahun kelahiran 25 Desember 1992 dari  Fakultas Ekonomi, putri dari Hasiolan Simalango / Rospita Br Sinaga.\r\n\r\nPERCAYA DIRI\r\nProf Syawal Gultom dalam orasi ilmiahnya yang berjudul "Membangun Indonesia Emas 2045" memaparkan, Indonesia memliki potensi yang besar. Berdasarkan survei independen, tahun 2030, Indonesia akan memperoleh peringkat ke-7 dunia sebagai negara pendidikan. \r\n\r\nSaat ini jumlah masyarakat terdidik di Indonesia mencapai 50 juta orang. Nantinya di tahun 2030, tenaga terdidik masyarakat Indonesia mencapai 110 juta orang. \r\nDia menjelaskan, tahun 2002 lalu, Amerika Serikat pernah melakukan riset dari 477 tenaga pemikir terkait bagaimana tingkat keberhasilan dari kelulusan perguruan tinggi. Dari hasil riset itu ada 20 hal penting dibutuhkan lulusan perguruan tinggi di antaranya, indeks prestasi, kemampuan berkomunikasi, networking atau jaringan kerja, mampu memotivasi, kreatif, inovatif, optimis, pantang menyerah, disiplin  dan  tanggung jawab. \r\n\r\n"Mengapa selama ini orang mau datang ke perguruan tinggi? Tujuannya sekarang ini merupakan langkah untuk mencapai pembelajaran yang sukses. Tetapi kemampuan berkomunikasi dan networkinglah yang menjadi prioritas bagi lulusan perguruan tinggi dalam mencapai masa depan. Sekarang ini bagaimana menempatkan diri menjadi pelajar yang sukses dan percaya diri," jelasnya. \r\n\r\nKarenanya dia menegaskan, saat ini seorang sarjana itu harus bisa yakinkan dirinya dan menjadi seorang yang percaya diri. Selain itu, seorang sarjana juga harus bisa menjadi wujud sebagai warga negara yang baik. "Disamping itu juga, seorang sarjana harus memiliki pribadi yang berketrampilan dan bertanggung jawab," ujarnya. \r\nPoin-poin itulah yang menjadi konstruksi bagi para sarjana untuk menghantarkan pembangunan Indonesia emas tahun 2045 (100 tahun kemerdekaan). \r\nAMALKAN KEPADA MASYARAKAT\r\n\r\nRektor Unita Ir Adriani Siahaan MP dalam sambutannya menyampaikan selamat kepada para wisudawan serta mengamanatkan, saat ini masyarakat menunggu tanggung jawab dari para sarjana yang baru diwisuda. Dibutuhkan potensi profesionalitas yang mana harus memiliki kreatifitas, inovatif, berkomunikasi dengan baik serta mampu mengambil keputusan yang cepat, ucapnya.  Ilmu sebagai alat, akan sangat berarti jika disadari kepada dan untuk apa serta bagaimana ilmu tersebut dapat bermanfaat bagi masyarakat secara keseluruhan. Sekarang saatnyalah, ilmu tersebut bisa diamalkan kepada masyarakat, pesan Adriani.\r\n"Dalam rangka memperbaiki tata kelola perguruan tinggi, UNITA berupaya terus menerapkan penjaminan mutu manajemen melalui kerja tersistem dan terintegrasi.\r\n\r\nDi samping itu, UNITA juga berperan memfasilitasi seminar dan ke depan mengadakan pelatihan kepada para kepala sekolah dan guru terkait regulasi pendidikan serta sisdiknas guna mendorong peningkatan kualitas SDM tenaga dan penyelenggaraan  pendidikan sekolah di lingkungan Tapanuli dengan menggandeng Dinas Pendidikan se kawasan Tapanuli. \r\n\r\nJANGAN BERHENTI BELAJAR\r\nPerwakilan Kopertis Wilayah I, Dra Elfriana MPd dalam sambutannya mengatakan, keberhasilan yang diraih para wisudawan tahun ini di Unita sudah dilalui dengan proses yang panjang. Tetapi menurutnya, keberhasilan yang diraih sekarang belum cukup. Keberhasilan itu harus dilalui dengan tahapan lanjutan. Salah satunya disiplin dan kemampuan berkomunikasi. \r\n\r\n"Jika itu semua terlaksana, semua harapan kedepan bisa terwujud. Tetapi apa yang diwujudkan nanti harus bisa menghasilkan peluang yang lebih baik lagi. Peluang yang harus ditingkatkan harus bisa meningkatkan kualitas dan kuantitas," ujarnya. \r\n\r\nMenurutnya, meningkatkan kualitas harus terus belajar. Karena belajar itu tidak akan mengenal waktu dan usia. Untuk itu, dia berharap kepada para seluruh wisudawan jangan pernah berhenti belajar. Apa yang dicapai sekarang jangan merasa puas. \r\n\r\nMEMBANGUN TAPUT  \r\nPada kesempatan itu juga, Bupati Tapanuli Utara Nikson Nababan dalam pidatonya menyampaikan selamat kepada seluruh para wisudawan yang telah mencapai keberhasilannya melalui perjuangan panjang. \r\n\r\nAtas keberhasilan itu, harapannya ke depan para sarjana yang telah diwisuda bisa menjadi insan yang berkualitas di lingkungan masyarakat. \r\nUntuk menyukseskan program itu, dia mengharapkan para wisudawan/wisudawati yang mendapat predikat Cum Laude untuk turut bergabung mengabdi membangun Kabupaten Taput. \r\n\r\nPENGABDIAN SARJANA\r\nSambutan mewakili tokoh masyarakat Sumatera Utara, mantan Sekdaprovsu dan Bupati Taput 1999-2004, RE Nainggolan, menyampaikan ucapan selamat kepada Rektor UNITA yang baru Ir Adriani Siahaan MP. Kiranya di tangan kepemipinan rektor yang baru ini, Unita bisa lebih meningkatkan mutunya yang lebih baik lagi. \r\n\r\nUntuk para sarjana yang baru diwisuda, dia mengharapkan supaya keterampilan dan kemampuannya diasah dengan baik. Sesuai  UU No 12 Tahun 2012, setiap sarjana di Indonesia dipacu untuk unggul dalam ketrampilan dan kemampuannya. \r\n\r\n"Kami yakin semua itu telah dimiliki adik-adik kami yang diwisuda sekarang. Saat ini, buktikanlah semua itu untuk meningkatkan kecerdasaan di tengah-tengah masyarakat," ujarnya. \r\n\r\nDia meminta para sarjana yang telah diwisuda bisa membuktikan semua keunggulannya khususnya kepada orang tua yang telah bersusah payah menyekolahkan sampai ke tingkat sarjana. Jadi tunjukkanlah pengabdian itu sekarang\r\n\r\n"Dalam kehidupan ini, ada dua pengabdian yang perlu kita jalankan yakni, pengabdian terhadap Tuhan dan pengabdian terhadap orang tua. Apalagi orang tua merupakan orang kedua dari Tuhan. Jadi saat ini merupakan waktu bagi adik-adik kami membuktikan pengabdiannya," ungkapnya. \r\n\r\nDia menegaskan, keberhasilan Tapanuli bisa menempah sarjana-sarjana yang terampil saat ini merupakan perjuangan dari Almarhum DR GM Panggabean untuk memajukan pendidikan di kawasan Tapanuli. \r\n\r\n"Kita lihat selama berpuluh tahun kampus ini berdiri sudah beribu sarjana diwisuda dan bisa membuktikan pengabdiannya untuk pembangunan bonapasogit. Untuk kita berterima kasih atas pemikiran dan gagasan dari Alm DR GM Panggabean membangun, puluhan ribu masyarakat di kawasan Tapanuli bisa ditempa menjadi sarjana-sarjana yang terampil," ujarnya. \r\n\r\nPada kesempatan itu RE juga memberikan apresiasi yang baik kepada Bupati Taput, Nikson Nababan, sebagai pemimpin yang masih muda, dinilainya memiliki pemikiran-pemikiran yang cemerlang untuk pembangunan Kabupaten Taput, ujarnya. \r\n\r\nUCAPAN TERIMAKASIH \r\nSementara mewakili  orang tua dari para wisudawan Walben Siahaan mengatakan, saat ini orang tua merasa senang dengan situasi ini. Harapan dan doa dari para orang tua saat ini sudah terwujud. \r\n\r\n"Tetapi harapan kami dari para orang tua, para sarjana ini bisa memberikan perhatiannya buat bonapasogit tercinta. Makanya kami harapkan, para anak-anak kami yang baru diwisuda bisa membuktikan penerapan ilmunya di tengah-tengah masyarakat," ujarnya. \r\n\r\nMenurutnya, selama ini di Indonesia lebih banyak perencanaannya tetapi pelaksanaannya tidak terealisasi. \r\n\r\nDi akhir acara, mewakili wisudawan Hendar Hutabarat mengucapkan terima kasih yang besar buat para pendidik di Unita yang telah menempah mereka menjadi sarjana yang terbaik. \r\n"Pendidikan inilah yang dapat menjadikan kami sebagai individu cendikiawan yang bermartabat di tengah-tengah masyarakat. Untuk itu, kami juga mengucapkan terima kasih kepada orang tua kami yang telah bersusah payah menjadikan kami menjadi orang terdidik. Kiranya ilmu yang kami dapat sekarang bisa kami terapkan dalam memajukan kecerdasan di Tapanuli," ucapnya.\r\n\r\n5000 HADIRIN MENIKMATI KAMPUS UNITA\r\nAcara wisuda dihadiri ribuan masyarakat, baik keluarga para wisudawan/i maupun masyarakat sekitar kampus UNITA Silangit. Walau acara wisuda berlangsung dalam waktu yang cukup lama karena banyaknya yang dilantik, tapi para keluarga wisudawan dengan setia mengikutinya.\r\n\r\nSebagian keluarga wisudawan  berasal dari desa-desa yang jauh tampak hadir ke kampus UNITA  membawa serangkaian menu tradisional Batak yang langsung disuguhkan sebagai upa-upa (ungkapan syukur) kepada putra-putri nya yang diwisuda seusai pelantikan. Halaman kampus yang sangat luas itu bahkan terlihat dipenuhi para hadirin yang begitu antusias mengikuti hari bahagia duduk  beralas tikar yang dibawa mereka di lapangan rumput bersama para sarjana UNITA ini. Hari itupun berubah menjadi hari tamasya keluarga. Bahkan para tamu, tak terkecuali Prof Syawal Gultom dari Kemenndikbud    terkagum-kagum dan haru menyaksikan suasana ini. Terlihat hingga ke sudut-sudut lokasi kampus menjadi tempat kumpul para pengunjung dengan menggelar tikar dan menikmati makan siang yang dibawa.', 'hariansib_Menciptakan-Sarjana-yang-Percaya-Diri-Jadi-Tantangan--.jpg', 'Ilmu Hukum', '2016-12-22', '4'),
(3, 'Mahasiswa UNITA Siborongborong Mogok Kuliah', 'Siborongborong ï¿½ Mahasiswa Universitas Sisingamangaraja XII (Unita) di Silangit, Kecamatan Siborongborong, Kabupaten Tapanuli Utara (Taput), melakukan aksi mogok kuliah selama seminggu. Bahkan, Kamis (18/11), ratusan mahasiswa mengancam akan terus melakukan aksi mogok kuliah, hingga tuntutan mereka dikabulkan. Yakni mengganti pembantu rektor 1 dan memperbaharui akreditasi status jurusan/fakultas FKIP, Teknik, dan Pertanian yang sudah kedaluwarsa.\r\n\r\nï¿½Kami akan terus melakukan aksi demo dan mogok kuliah sampai tuntutan kami dikabulkan oleh pihak yayasan. Kita menginginkan apa yang menjadi hak kita. Sebab, kita juga menyampaikan kewajiban kita kuliah di kampus ini,ï¿½ ujar Juandi Sihombing sebagai penaggung jawab aksi, kepada wartawan, Kamis (18/11) di Silangit.\r\n\r\nDijelaskannya, para mahasiswa saat ini terus mendesak legalisasi  akreditasi sejumlah jurusan di kampus tersebut. Di antaranya, akreditasi status jurusan/fakultas FKIP, Teknik, dan Pertanian yang sudah kadaluarsa.\r\n\r\nDisi lain, Juandi memaparkan, fasilitas sarana penunjang pendidikan di kampus tersebut juga tidak mendukung. ï¿½Kita ke sini kuliah bukan gratis.\r\n\r\nTapi apa yang kita dapatkan di kampus ini tidak sesuai dengan apa yang menjadi hak kita. Semisal, toilet kampus saja tidak beres, belum lagi banyaknya persoalan diskriminasi,ï¿½ tukasnya.\r\n\r\nAtas terkedaluwarsanya akreditasi tersebut, mahasiswa mendesak pihak yayasan agar segera mengganti pembantu Rektor I (satu), Adriani Siahaan. Sebab, menurut para mahasiswa, kedaluwarsanya akreditasi tersebut disebabkan oleh kelalaian pembantu Rektor I.\r\n\r\nSebelumnya, pihak Yayasan Universitas Sisingamangaraja XII, melalui sekretaris yayasan, Tuty Rotua Panggabean kepada para mahasiswa menjelaskan, bahwa akreditasi jurusan yang sudah habis izinnya tersebut, saat ini masih dalam tahap pengurusan.\r\n\r\nDi mana pihaknya sudah mengajukan perpanjangan akreditasi sebelum habis masa berlaku ke Dirjen Pendidikan Tinggi. ï¿½Jadi akreditasi jurusan itu masih legal. Sebab, ada perpanjangan bagi kampus yang belum terdata perpanjangan akreditasinya di Dirjen Dikti,ï¿½ jelas Tuty.\r\n\r\nMenanggapi sejumlah permasalahan di kampus Unita, salahseorang orangtua mahasiswa, warga Doloksanggul, Kabupaten Humbang Hasundutan (Humbahas), yang enggan disebutkan namanya kepada wartawan, Kamis (18/11) mengimbau agar mahasiswa kembali melakukan kuliah seperti biasa. Sebab, menurutnya, dengan melakukan aksi mogok kuliah, hanyalah merugikan mahasiswa itu sendiri.\r\n\r\nï¿½Saya prihatin melihat cara-cara mahasiswa Unita itu. Tidak harus demo untuk menyelesaikan masalah, bisa dengan cara terhormat.\r\n\r\nTidak ada masalah yang tidak bisa diselesaikan. Jadi saya berharap mereka (mahasiwa-red) kembali kuliah seperti biasa. Sebab, saya sendiri khawatir, anak saya ikut demo yang kita nilai tidak berguna itu,ï¿½ harapnya.', 'mogok.jpg', 'Umum', '2016-12-26', '14'),
(4, 'Selamat datang di Universitas Tapanuli Utara', 'Silangit, Taput - Pengukuhan mahasiswa/i baru Universitas Sisingamangaraja XII Tapanuli (UNITA) tahun ajaran 2014-2015 melaksanakan Orientasi pengenalan kampus (Ospek) yang dilaksanakan 24-26 September 2014 mempertunjukkan kearifan lokal budaya batak di Kampus Bumi Pendidikan Silangit Kabupaten Tapanuli Utara.\r\n\r\nPengukuhan mahasiswa/i baru UNITA dibuka oleh Rektor Ir Adriani Siahaan MP didampingi PR II Arta Hutahaean SP MSi, Ketua Penerimaan Mahasiswa Baru (PMB) Dra Jumaria Sirait MPd, Kordinator Etalase Budaya Drs Bonari Tambunan MHum dan utusan tokoh masyarakat Toga Simanjuntak.\r\n\r\nSebelum pengukuhan, di halaman gedung utama kampus UNITA yang merupakan salah satu ikon Tapanuli, terlihat seluruh mahasiswa/i baru berbaris di tengah lapangan mengikuti rangkaian kegiatan yang diatur panitia Ospek.\r\n\r\nSelama kegiatan Ospek, para mahasiswa senior selalu menunjukkan perlakuan simpatik dan mengajarkan cara bergaul yang baik di kampus kepada para mahasiswa/i baru guna membangun hubungan yang sinergis antara mahasiswa senior dan junior yang dapat bermanfaat serta tetap selalu mengarahkan juniornya bagaimana menciptakan suasana kampus yang nyaman agar mahasiswa bisa menciptakan prestasi dalam perkuliahan.\r\n\r\nMenurut mahasiswa/i baru UNITA, Ospek itu penting untuk pengenalan dirinya ke mahasiswa yang sama-sama baru. Mahasiswa baru juga bisa mengikuti berbagai tes atau permainan yang menguji kekompakan, kesetiakawanan dan solidaritas. \r\n\r\nKetua PMB Dra Jumaria Sirait MPd mengatakan, kearifan lokal dalam acara pengukuhan mahasiswa/i baru UNITA itu adalah gagasan yang bersifat bijaksana, penuh kearifan, bernilai baik dan didukung oleh anggota masyarakatnya.\r\n\r\nDikatakan, adat budaya daerah termasuk adat budaya Batak dapat digunakan sebagai sarana dalam mempertahankan integrasi dan identitas bangsa, terutama di kalangan suku-suku yang ada di daerah secara luas. ï¿½Di sana-sini pelaksanaan adat di daerah  semakin mengalami pendangkalan. Pendangkalan dimaksud ditandai dengan semakin berkurangnya perhatian masyarakat generasi muda mengenal dan melaksanakannya,ï¿½ imbuhnya.\r\n\r\nDengan begitu lanjutnya, diperlukan kerjasama yang baik dari semua pihak dalam memberi pengenalan dan pengamalan adat budaya daerah sebagai budaya nasional. Pembinaan adat budaya daerah dalam situasi bangsa menghadapi tantangan globalisasi diperlukan guna untuk menumbuhkan dan menguatkan kembali budaya Batak tersebut seperti yang selalu dilakukan Alm DR GM Panggabean (pendiri yayasan Sisingamangaraja XII) semasa hidupnya.\r\n\r\nBapak Alm DR GM Panggabean seorang pekerja keras, khususnya dalam mengelola UNITA dan sangat wajar kalau Bapak Alm DR Panggabean diapresiasi seluruh masyarakat Tapanuli dan secara luas oleh masyarakat Indonesia karena kecintaannya terhadap budaya Batak.\r\n\r\nTerpisah, Rektor Ir Adriani Siahaan MP dalam arahannya mengatakan, kepada mahasiswa/i UNITA yang sudah selesai mengikuti Ospek agar jangan merasa terbebani dengan adanya acara Ospek itu. ï¿½Disinilah Orientasi studi pengenalan kampus diawali, yang dapat menjadikan mahasiswa/i baru lebih giat belajar dan berprestasi dalam menuntut ilmu baik di bidang akademik dan non akademik, ungkapnya.\r\n\r\nRektor menambahkan, Ospek UNITA juga lebih mengarah kepada kegiatan yang perlu diberikan pada calon mahasiswa dan lebih banyak bimbingan dan pembinaan sehingga acara Ospek tersebut bermanfaat bagi kepribadian serta bisa menyelesaikan kuliah tepat waktu dan menjadi lulusan yang mandiri, bermutu dan berkarakter seperti Visi UNITA.\r\n\r\nKegiatan Ospek yang menggunakan aksesoris yang cukup unik seperti, kaos kaki yang berbeda warna, topi yang terbuat kertas, tas yang terbuat dari goni serta rambut yang diikat menjadi beberapa bagian, ini semua dilakukan untuk menguji mental para mahasiswa baru, ucapnya seraya mengucapkan selamat datang kepada mahasiswa/i baru.\r\n\r\nAdapun tokoh masyarakat Desa Silangit yang masing-masing didampingi istri memasuki Kampus UNITA dengan menjunjung ï¿½tandokï¿½ berisikan boras sipir ni tondi yang mengartikan semangat rasa turut memiliki Kampus UNITA yang berada di lingkungan desa mereka antara lain, Toga Simanjuntak, Nursamen Simaremare, R Sinaga, A Tampubolon, Sumihar Tampubolon dan Risma Hutasoit.\r\n\r\nSelanjutnya para mahasiswa/i senior (panitia Ospek) menyambut  para fungsionaris UNITA, tokoh masyarakat dengan tor-tor menuju Aula UNITA tempat pelaksanaan acara pengukuhan dan pengukuhan mahasiswa/i baru pun dilaksanakan dengan pemberian ï¿½boras sipir ni tondiï¿½ dan ulos kepada setiap perwakilan dari seluruh mahasiswa yang mengikuti Ospek yang terdaftar di FKIP jurusan Matematika, Bahasa Inggris, Bahasa Indonesia dan PPKN, Fakultas Ekonomi jurusan Manajemen, Fakultas Hukum jurusan Ilmu Hukum, Fakultas Teknik jurusan Industri dan Fakultas Pertanian jurusan Budidaya Pertanian.\r\n\r\nSelain itu para fungsionaris UNITA dan tokoh masyarakat selanjutnya memberikan dekke simudur-udur (ikan mas arsik), itak gur-gur, tuak takkasan minuman khas Batak.\r\n\r\nPengukuhan diakhiri dengan pelepasan aksesoris Ospek di lapangan depan kampus UNITA yang digunakan calon mahasiswa-calon mahasiswi (Cama-Cami) selama 2 hari dan selanjutnya dibakar. Ospek diakhiri acara peserta dan panitia membaur jadi satu untuk berjoget ria dengan lantunan lagu Kemesraan dan kemudian dilanjutkan dengan makan bersama.', '13418828_113620145729480_8705762089081795484_n.jpg', 'Umum', '2017-01-12', '13'),
(5, 'Horas', 'Silangit, Taput - Pengukuhan mahasiswa/i baru Universitas Sisingamangaraja XII Tapanuli (UNITA) tahun ajaran 2014-2015 melaksanakan Orientasi pengenalan kampus (Ospek) yang dilaksanakan 24-26 September 2014 mempertunjukkan kearifan lokal budaya batak di Kampus Bumi Pendidikan Silangit Kabupaten Tapanuli Utara.\r\n\r\nPengukuhan mahasiswa/i baru UNITA dibuka oleh Rektor Ir Adriani Siahaan MP didampingi PR II Arta Hutahaean SP MSi, Ketua Penerimaan Mahasiswa Baru (PMB) Dra Jumaria Sirait MPd, Kordinator Etalase Budaya Drs Bonari Tambunan MHum dan utusan tokoh masyarakat Toga Simanjuntak.\r\n\r\nSebelum pengukuhan, di halaman gedung utama kampus UNITA yang merupakan salah satu ikon Tapanuli, terlihat seluruh mahasiswa/i baru berbaris di tengah lapangan mengikuti rangkaian kegiatan yang diatur panitia Ospek.\r\n\r\nSelama kegiatan Ospek, para mahasiswa senior selalu menunjukkan perlakuan simpatik dan mengajarkan cara bergaul yang baik di kampus kepada para mahasiswa/i baru guna membangun hubungan yang sinergis antara mahasiswa senior dan junior yang dapat bermanfaat serta tetap selalu mengarahkan juniornya bagaimana menciptakan suasana kampus yang nyaman agar mahasiswa bisa menciptakan prestasi dalam perkuliahan.\r\n\r\nMenurut mahasiswa/i baru UNITA, Ospek itu penting untuk pengenalan dirinya ke mahasiswa yang sama-sama baru. Mahasiswa baru juga bisa mengikuti berbagai tes atau permainan yang menguji kekompakan, kesetiakawanan dan solidaritas. \r\n\r\nKetua PMB Dra Jumaria Sirait MPd mengatakan, kearifan lokal dalam acara pengukuhan mahasiswa/i baru UNITA itu adalah gagasan yang bersifat bijaksana, penuh kearifan, bernilai baik dan didukung oleh anggota masyarakatnya.\r\n\r\nDikatakan, adat budaya daerah termasuk adat budaya Batak dapat digunakan sebagai sarana dalam mempertahankan integrasi dan identitas bangsa, terutama di kalangan suku-suku yang ada di daerah secara luas. ï¿½Di sana-sini pelaksanaan adat di daerah  semakin mengalami pendangkalan. Pendangkalan dimaksud ditandai dengan semakin berkurangnya perhatian masyarakat generasi muda mengenal dan melaksanakannya,ï¿½ imbuhnya.\r\n\r\nDengan begitu lanjutnya, diperlukan kerjasama yang baik dari semua pihak dalam memberi pengenalan dan pengamalan adat budaya daerah sebagai budaya nasional. Pembinaan adat budaya daerah dalam situasi bangsa menghadapi tantangan globalisasi diperlukan guna untuk menumbuhkan dan menguatkan kembali budaya Batak tersebut seperti yang selalu dilakukan Alm DR GM Panggabean (pendiri yayasan Sisingamangaraja XII) semasa hidupnya.\r\n\r\nBapak Alm DR GM Panggabean seorang pekerja keras, khususnya dalam mengelola UNITA dan sangat wajar kalau Bapak Alm DR Panggabean diapresiasi seluruh masyarakat Tapanuli dan secara luas oleh masyarakat Indonesia karena kecintaannya terhadap budaya Batak.\r\n\r\nTerpisah, Rektor Ir Adriani Siahaan MP dalam arahannya mengatakan, kepada mahasiswa/i UNITA yang sudah selesai mengikuti Ospek agar jangan merasa terbebani dengan adanya acara Ospek itu. ï¿½Disinilah Orientasi studi pengenalan kampus diawali, yang dapat menjadikan mahasiswa/i baru lebih giat belajar dan berprestasi dalam menuntut ilmu baik di bidang akademik dan non akademik, ungkapnya.\r\n\r\nRektor menambahkan, Ospek UNITA juga lebih mengarah kepada kegiatan yang perlu diberikan pada calon mahasiswa dan lebih banyak bimbingan dan pembinaan sehingga acara Ospek tersebut bermanfaat bagi kepribadian serta bisa menyelesaikan kuliah tepat waktu dan menjadi lulusan yang mandiri, bermutu dan berkarakter seperti Visi UNITA.\r\n\r\nKegiatan Ospek yang menggunakan aksesoris yang cukup unik seperti, kaos kaki yang berbeda warna, topi yang terbuat kertas, tas yang terbuat dari goni serta rambut yang diikat menjadi beberapa bagian, ini semua dilakukan untuk menguji mental para mahasiswa baru, ucapnya seraya mengucapkan selamat datang kepada mahasiswa/i baru.\r\n\r\nAdapun tokoh masyarakat Desa Silangit yang masing-masing didampingi istri memasuki Kampus UNITA dengan menjunjung ï¿½tandokï¿½ berisikan boras sipir ni tondi yang mengartikan semangat rasa turut memiliki Kampus UNITA yang berada di lingkungan desa mereka antara lain, Toga Simanjuntak, Nursamen Simaremare, R Sinaga, A Tampubolon, Sumihar Tampubolon dan Risma Hutasoit.\r\n\r\nSelanjutnya para mahasiswa/i senior (panitia Ospek) menyambut  para fungsionaris UNITA, tokoh masyarakat dengan tor-tor menuju Aula UNITA tempat pelaksanaan acara pengukuhan dan pengukuhan mahasiswa/i baru pun dilaksanakan dengan pemberian ï¿½boras sipir ni tondiï¿½ dan ulos kepada setiap perwakilan dari seluruh mahasiswa yang mengikuti Ospek yang terdaftar di FKIP jurusan Matematika, Bahasa Inggris, Bahasa Indonesia dan PPKN, Fakultas Ekonomi jurusan Manajemen, Fakultas Hukum jurusan Ilmu Hukum, Fakultas Teknik jurusan Industri dan Fakultas Pertanian jurusan Budidaya Pertanian.\r\n\r\nSelain itu para fungsionaris UNITA dan tokoh masyarakat selanjutnya memberikan dekke simudur-udur (ikan mas arsik), itak gur-gur, tuak takkasan minuman khas Batak.\r\n\r\nPengukuhan diakhiri dengan pelepasan aksesoris Ospek di lapangan depan kampus UNITA yang digunakan calon mahasiswa-calon mahasiswi (Cama-Cami) selama 2 hari dan selanjutnya dibakar. Ospek diakhiri acara peserta dan panitia membaur jadi satu untuk berjoget ria dengan lantunan lagu Kemesraan dan kemudian dilanjutkan dengan makan bersama.', '1.png', 'Pertanian', '2017-01-12', '12'),
(6, 'Taput', 'Silangit, Taput - Pengukuhan mahasiswa/i baru Universitas Sisingamangaraja XII Tapanuli (UNITA) tahun ajaran 2014-2015 melaksanakan Orientasi pengenalan kampus (Ospek) yang dilaksanakan 24-26 September 2014 mempertunjukkan kearifan lokal budaya batak di Kampus Bumi Pendidikan Silangit Kabupaten Tapanuli Utara.\r\n\r\nPengukuhan mahasiswa/i baru UNITA dibuka oleh Rektor Ir Adriani Siahaan MP didampingi PR II Arta Hutahaean SP MSi, Ketua Penerimaan Mahasiswa Baru (PMB) Dra Jumaria Sirait MPd, Kordinator Etalase Budaya Drs Bonari Tambunan MHum dan utusan tokoh masyarakat Toga Simanjuntak.\r\n\r\nSebelum pengukuhan, di halaman gedung utama kampus UNITA yang merupakan salah satu ikon Tapanuli, terlihat seluruh mahasiswa/i baru berbaris di tengah lapangan mengikuti rangkaian kegiatan yang diatur panitia Ospek.\r\n\r\nSelama kegiatan Ospek, para mahasiswa senior selalu menunjukkan perlakuan simpatik dan mengajarkan cara bergaul yang baik di kampus kepada para mahasiswa/i baru guna membangun hubungan yang sinergis antara mahasiswa senior dan junior yang dapat bermanfaat serta tetap selalu mengarahkan juniornya bagaimana menciptakan suasana kampus yang nyaman agar mahasiswa bisa menciptakan prestasi dalam perkuliahan.\r\n\r\nMenurut mahasiswa/i baru UNITA, Ospek itu penting untuk pengenalan dirinya ke mahasiswa yang sama-sama baru. Mahasiswa baru juga bisa mengikuti berbagai tes atau permainan yang menguji kekompakan, kesetiakawanan dan solidaritas. \r\n\r\nKetua PMB Dra Jumaria Sirait MPd mengatakan, kearifan lokal dalam acara pengukuhan mahasiswa/i baru UNITA itu adalah gagasan yang bersifat bijaksana, penuh kearifan, bernilai baik dan didukung oleh anggota masyarakatnya.\r\n\r\nDikatakan, adat budaya daerah termasuk adat budaya Batak dapat digunakan sebagai sarana dalam mempertahankan integrasi dan identitas bangsa, terutama di kalangan suku-suku yang ada di daerah secara luas. ï¿½Di sana-sini pelaksanaan adat di daerah  semakin mengalami pendangkalan. Pendangkalan dimaksud ditandai dengan semakin berkurangnya perhatian masyarakat generasi muda mengenal dan melaksanakannya,ï¿½ imbuhnya.\r\n\r\nDengan begitu lanjutnya, diperlukan kerjasama yang baik dari semua pihak dalam memberi pengenalan dan pengamalan adat budaya daerah sebagai budaya nasional. Pembinaan adat budaya daerah dalam situasi bangsa menghadapi tantangan globalisasi diperlukan guna untuk menumbuhkan dan menguatkan kembali budaya Batak tersebut seperti yang selalu dilakukan Alm DR GM Panggabean (pendiri yayasan Sisingamangaraja XII) semasa hidupnya.\r\n\r\nBapak Alm DR GM Panggabean seorang pekerja keras, khususnya dalam mengelola UNITA dan sangat wajar kalau Bapak Alm DR Panggabean diapresiasi seluruh masyarakat Tapanuli dan secara luas oleh masyarakat Indonesia karena kecintaannya terhadap budaya Batak.\r\n\r\nTerpisah, Rektor Ir Adriani Siahaan MP dalam arahannya mengatakan, kepada mahasiswa/i UNITA yang sudah selesai mengikuti Ospek agar jangan merasa terbebani dengan adanya acara Ospek itu. ï¿½Disinilah Orientasi studi pengenalan kampus diawali, yang dapat menjadikan mahasiswa/i baru lebih giat belajar dan berprestasi dalam menuntut ilmu baik di bidang akademik dan non akademik, ungkapnya.\r\n\r\nRektor menambahkan, Ospek UNITA juga lebih mengarah kepada kegiatan yang perlu diberikan pada calon mahasiswa dan lebih banyak bimbingan dan pembinaan sehingga acara Ospek tersebut bermanfaat bagi kepribadian serta bisa menyelesaikan kuliah tepat waktu dan menjadi lulusan yang mandiri, bermutu dan berkarakter seperti Visi UNITA.\r\n\r\nKegiatan Ospek yang menggunakan aksesoris yang cukup unik seperti, kaos kaki yang berbeda warna, topi yang terbuat kertas, tas yang terbuat dari goni serta rambut yang diikat menjadi beberapa bagian, ini semua dilakukan untuk menguji mental para mahasiswa baru, ucapnya seraya mengucapkan selamat datang kepada mahasiswa/i baru.\r\n\r\nAdapun tokoh masyarakat Desa Silangit yang masing-masing didampingi istri memasuki Kampus UNITA dengan menjunjung ï¿½tandokï¿½ berisikan boras sipir ni tondi yang mengartikan semangat rasa turut memiliki Kampus UNITA yang berada di lingkungan desa mereka antara lain, Toga Simanjuntak, Nursamen Simaremare, R Sinaga, A Tampubolon, Sumihar Tampubolon dan Risma Hutasoit.\r\n\r\nSelanjutnya para mahasiswa/i senior (panitia Ospek) menyambut  para fungsionaris UNITA, tokoh masyarakat dengan tor-tor menuju Aula UNITA tempat pelaksanaan acara pengukuhan dan pengukuhan mahasiswa/i baru pun dilaksanakan dengan pemberian ï¿½boras sipir ni tondiï¿½ dan ulos kepada setiap perwakilan dari seluruh mahasiswa yang mengikuti Ospek yang terdaftar di FKIP jurusan Matematika, Bahasa Inggris, Bahasa Indonesia dan PPKN, Fakultas Ekonomi jurusan Manajemen, Fakultas Hukum jurusan Ilmu Hukum, Fakultas Teknik jurusan Industri dan Fakultas Pertanian jurusan Budidaya Pertanian.\r\n\r\nSelain itu para fungsionaris UNITA dan tokoh masyarakat selanjutnya memberikan dekke simudur-udur (ikan mas arsik), itak gur-gur, tuak takkasan minuman khas Batak.\r\n\r\nPengukuhan diakhiri dengan pelepasan aksesoris Ospek di lapangan depan kampus UNITA yang digunakan calon mahasiswa-calon mahasiswi (Cama-Cami) selama 2 hari dan selanjutnya dibakar. Ospek diakhiri acara peserta dan panitia membaur jadi satu untuk berjoget ria dengan lantunan lagu Kemesraan dan kemudian dilanjutkan dengan makan bersama.', 'COLLECTIE_TROPENMUSEUM_Een_overstroming_van_de_Silindoengvlakte_bij_Taroetoeng_TMnr_60021568.jpg', 'Pendidikan', '2017-01-12', '34'),
(7, 'Cobacoba', 'Hellowworld Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Hellowworld Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Hellowworld Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!Hellowworld Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis aliquid atque, nulla? Quos cum ex quis soluta, a laboriosam. Dicta expedita corporis animi vero voluptate voluptatibus possimus, veniam magni quis!', 'buildMLirethil.PNG', 'Manajemen', '2017-11-22', '84'),
(8, 'EPC Tentang Teknik Sipil', 'Kita awali dulu dengan kepanjangannya, EPC adalah singkatan dari istilah Engineering-Procurement-Construction.\r\n\r\nKalo dilihat dari istilah, EPC itu tidak lain adalah tahapan dalam suatu proyek konstruksi. Engineering adalah tahap desain perencanaan, Procurement adalah tahap pengadaan barang dan jasa, dan Construction adalah tahap pelaksanaan konstruksi.\r\n\r\nAda yang menarik di sini, yaitu tahap Procurement jarang ditemukan di dalam proyek biasa. Kalo proyek biasa, kita hanya mengenal tahap Perencanaan (Desain) dan tahap Pelaksanaan (Konstruksi). Lalu? Kenapa harus ada tahap Procurement? Jawabannya sederhana, karena proyeknya bukan proyek biasa.\r\n\r\nYup. Sistem EPC memang dipake di hampir sebagian besar proyek konstruksi yang â€œtidak biasaâ€, misalnya pada Industri Migas, Pembangkit Tenaga Listrik & Energi, Pertambangan, dan jenis industri berat lainnya. Sementara proyek yang kita anggap sebagai proyek biasa yaitu berbagai jenis bangunan gedung dan industri skala kecil ngga perlu menggunakan sistem EPC, malah bisa bikin susah.', 'ant.PNG', 'Teknik Sipil', '2017-12-08', '32');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `artikel_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `comment` text NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`id`, `artikel_id`, `name`, `email`, `comment`, `date`) VALUES
(4, 7, 'ojack', 'ojackbutar@gmail.com', 'good', '2017-12-26 16:47:42');

-- --------------------------------------------------------

--
-- Table structure for table `galeri`
--

CREATE TABLE `galeri` (
  `id_galeri` int(5) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `galeri`
--

INSERT INTO `galeri` (`id_galeri`, `judul`, `gambar`, `tanggal`) VALUES
(6, 'Wisudawan Universitas Tapanuli Utara', 'hariansib_Menciptakan-Sarjana-yang-Percaya-Diri-Jadi-Tantangan--.jpg', '2017-01-03'),
(7, 'Logo UNITA', 'unita.jpg', '2017-01-03'),
(8, 'Manortor ', 'tortor.jpg', '2017-01-03'),
(9, 'Coba', 'buildMLzilong.PNG', '2017-11-22'),
(10, 'gdfgfdgggg', 'download antivirus gratis terbaik.png', '2017-12-02');

-- --------------------------------------------------------

--
-- Table structure for table `halaman`
--

CREATE TABLE `halaman` (
  `id_halaman` int(5) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `halaman`
--

INSERT INTO `halaman` (`id_halaman`, `judul`, `isi`) VALUES
(3, 'Profil', 'Universitas Sisingamangaraja XII Tapanuli Utara\r\n\r\nBadan Hukum	:Yayasan Pendidikan Sisingamangaraja XII\r\nBerdiri	        :29 Juli 1985\r\nAkte Pendirian	:17 Januari 1987\r\nAlamat	        :Jl Perintis Kemerdekaan No 9 Medan 20235\r\nTelepon	        :061-4146659\r\nFaksimili	:061-4539642\r\nPengurus\r\nKetua    	:DR GM PANGGABEAN\r\nSekretaris	:GELORA MARINTHAN SIA PANGGABEAN\r\nBendahara	:NETTY VERA MEGAWATI PANGGABEAN\r\n	\r\nPerguruan Tinggi\r\nBerdiri	        :2 April 1986\r\nAlamat	        :Jl Sisingamangaraja XII No 9 Silangit Siborongborong 22474\r\nTelepon	        :0633-41017\r\nFaksimili	:0633-41017\r\n	\r\nRektor/Ketua/Direktur	:	PARSAORAN PARAPAT, M.Si\r\nPembantu/Wakil I	:	ADRIANI SAURNIDA ASIANNA S, M.P.\r\nPembantu/Wakil II	:	SELAMAT SILITONGA, M.S.\r\nPembantu/Wakil III	:	DONVER PANGGABEAN, M.Si'),
(4, 'Menu', 'Halaman'),
(9, 'judul', 'hbhcsbcscb'),
(10, 'Visi dan Misi', 'MISI :  Universitas yang unggul  dalam bidang pendidikan,industri,dan \r\n        pariwisata.\r\n\r\nVISI :  1.Menyelenggarakan Pendidikan,Penelitian,Pengabdian kepada \r\n          Masyarakat.\r\n\r\n        2.Mengembangkan Kebudayaan Kewirausahaan.\r\n\r\n        3.Membina Suasana Akademik dan iklim Organisasi yang sehat. ');

-- --------------------------------------------------------

--
-- Table structure for table `konter`
--

CREATE TABLE `konter` (
  `ip` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `hits` int(10) NOT NULL,
  `online` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `konter`
--

INSERT INTO `konter` (`ip`, `tanggal`, `hits`, `online`) VALUES
('::1', '2017-12-21', 21, 1513851047),
('::1', '2017-12-22', 17, 1513948320),
('::1', '2017-12-24', 4, 1514125632),
('::1', '2017-12-26', 106, 1514283400),
('::1', '2017-12-27', 16, 1514388951),
('::1', '2017-12-30', 44, 1514598850),
('::1', '2018-01-02', 13, 1514893149),
('::1', '2018-01-03', 356, 1514996046);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id_menu` int(5) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  `urutan` varchar(50) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id_menu`, `judul`, `link`, `urutan`, `tanggal`) VALUES
(10, 'Profil', '?tampil=halaman&id=3', '2', '0000-00-00'),
(12, 'Visi dan Misi', '?tampil=halaman&id=10', '3', '0000-00-00'),
(13, 'Galeri', '?tampil=galeri', '4', '0000-00-00'),
(14, 'Kritik & Saran', '?tampil=kontak', '5', '0000-00-00'),
(15, 'Home', 'index.php', '1', '0000-00-00'),
(16, 'Alumni', '?tampil=alumni', '6', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id_pesan` int(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `subjek` varchar(50) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pesan`
--

INSERT INTO `pesan` (`id_pesan`, `nama`, `email`, `subjek`, `pesan`, `tanggal`) VALUES
(13, 'ojack', 'ojack@gmail.com', 'tentangkau', 'hellowakausdas', '2017-11-22');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(5) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`) VALUES
(2, 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agenda`
--
ALTER TABLE `agenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`id_foto`);

--
-- Indexes for table `artikel`
--
ALTER TABLE `artikel`
  ADD PRIMARY KEY (`id_artikel`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id_galeri`);

--
-- Indexes for table `halaman`
--
ALTER TABLE `halaman`
  ADD PRIMARY KEY (`id_halaman`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id_menu`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agenda`
--
ALTER TABLE `agenda`
  MODIFY `id_agenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `artikel`
--
ALTER TABLE `artikel`
  MODIFY `id_artikel` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id_galeri` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `halaman`
--
ALTER TABLE `halaman`
  MODIFY `id_halaman` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id_menu` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id_pesan` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
